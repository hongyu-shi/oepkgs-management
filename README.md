# oepkgs-management

## 介绍
我们构建一个src-oepkgs组织仓，作为第三方软件包代码托管平台，oepkgs-management仓库，包含组织仓src-oepkgs下仓库的配置文件，仓库结构如下：

        .
        ├── README
        └── sig
            ├── bigdata
            │   ├── sig-info.yaml
            │   └── src-oepkgs
            │       ├── a
            │       │   └── xx.yaml
            │       ├── b
            │       ├── c
            │       └── d
            ├── undefined
            ├── virtual
            └── web

#### 一、基于PR，创建仓库
在[oepkgs-management](https://gitee.com/oepkgs/oepkgs-management)仓库提PR(如何提PR，详见文档最后的[QA](##QA))，填写两个配置文件，PR合入之后，创仓机器人ci-rebot会在[src-oepkgs](https://gitee.com/src-oepkgs)下面自动创建仓库。

oepkgs-management仓库中的两个配置文件(以qemu为例)：
```
# 在oepkgs-management仓库sig目录下面创建虚拟化领域的sig组
# 创建oepkgs-management/sig/virtual/sig-info.yaml文件
oepkgs-management/sig/virtual/sig-info.yaml:

# sig组名称，一般跟软件包领域相关
name: virtual
description: "To support the field of virtual"
mailing_list: NA
meeting_url: NA
mature_level: startup
# sig组的管理者
maintainers:
- gitee_id: lipingEmmaSiguyi
  name: Ping Li
  orgnization: Huawei
  email: liping136@huawei.com
# 该sig组管理的仓库
repositories:
- repo: 
  - src-oepkgs/qemu   
```

```
# 在oepkgs-management/sig/virtual下面创建src-oepkgs/仓库名称首字母/仓库名称.yaml
# ci-rebot将依据这个文件进行自动建仓
oepkgs-management/sig/virtual/src-oepkgs/q/qemu.yaml:

# 仓库名称
name: qemu
description: "QEMU is a generic and open source processor emulator which achieves a good emulation speed by using dynamic translation"
# 仓库地址
upstream: https://gitee.com/src-oepkgs/qemu
# 仓库分支
branches:
- name: master
  type: protected
- name: openEuler-20.03-LTS-SP3
  type: protected
  create_from: master
- name: openEuler-22.03-LTS
  type: protected
  create_from: master
type: public
```
#### 二、补充源码文件
完成步骤一之后，5分钟内会生成https://gitee.com/src-oepkgs/qemu 仓库，通过PR往这个仓库中补充源码文件：

分别是可用于支撑生成rpm包的qemu.spec文件、软件包源码包qemu-2.12.0.tar.bz2，详见：https://gitee.com/src-oepkgs

提了PR之后，在5~30分钟时间内，会进行PR门禁构建测试，PR会评论出PR构建结果，建议在**Build_Result**显示为**SUCCESS**之后合入PR



前面配置文件oepkgs-management/sig/virtual/sig-info.yaml中指定的maintanier，可通过在PR下面评论/lgtm及/approve合入PR


PR合入之后会在合入的commit下面给出构建测试结果，以及安装测试结果，以及软件包上传到目标内部测试仓库**testing_repo**中,如果测试结果成功，第二天会更新至**oepkgs_reop**中(注：oepkgs仓库每日零点定时更新，测试结果中只是给出预计要存放的oepkgs仓库地址)：

#### 三、Check List
 在提交pr建仓时，为了避免出现bug导致建仓失败，需要对提交的pr进行自检。
| 检查项  | 简要说明 | 自检结果（是/否/不涉及）
|  ----  | ----  | ----  |
| 仓库名  | 1.仓库名不能以数字开头<br>2.仓库名必须与yaml文件名一致<br>3.仓库名不能重复(请先确认src-oepkgs中是否存在同名的仓库) |   |
| yaml文件  | 必须符合yaml格式规范（通过yaml语法及格式校验） |   |
| 目录层级  | 目录层级必须和其他已建仓库一致 |   |
| 是否完成自验 | 是否完成自验 |   |
