import yaml
import time
import subprocess

count = 0

with open("path", "r") as f_path:
    for line in f_path:
        count += 1
        print(count, line.strip())
        add_branch = {'name': 'openEuler-20.03-LTS-SP4', 'type': 'protected', 'create_from': 'openEuler-20.03-LTS-SP3'}
        with open(line.strip(), "r") as f_yaml_in:
            data = yaml.safe_load(f_yaml_in)
        if "sig-info.yaml" in line.strip():
            continue
        branches = data["branches"]
        branch_list = []
        for branch in data["branches"]:
            branch_list.append(branch["name"])
        if "openEuler-20.03-LTS-SP3" not in branch_list:
            continue
        if "openEuler-20.03-LTS-SP4" in branch_list:
            continue
        print(branch_list)
        data["branches"].append(add_branch)

        with open(line.strip(), "w") as f_yaml_out:
            yaml.safe_dump(data, f_yaml_out, sort_keys=False)

        if count % 100 == 0:
            # 执行外部命令
            process = subprocess.Popen(['sh', 'create_pr.sh'], stdout=subprocess.PIPE, text=True,
                                       cwd="/home/wcy/oepkgs-management")
            # 逐行读取输出
            for output in process.stdout:
                print(output.strip())
            print(f"已读取{count}, 暂停60分钟。。。")
            time.sleep(3600)

print("读取完毕")

